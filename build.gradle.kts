/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

plugins {
    `maven-publish`
    `kotlin-dsl`
    kotlin("jvm") version "1.4.10"
}

group = "unbekannt"
version = "0.0.2"

repositories {
    mavenCentral()
}

java{
    @Suppress("UnstableApiUsage")
    withSourcesJar()
}

tasks.test{
    useJUnitPlatform()
}

dependencies {
    testImplementation("org.junit.jupiter:junit-jupiter-api:5.6.2")
    testRuntimeOnly( "org.junit.jupiter:junit-jupiter-engine:5.6.2")
}

tasks.compileKotlin{
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs += "-Xjvm-default=all-compatibility"
}

tasks.compileTestKotlin{
    kotlinOptions.jvmTarget = "1.8"
    kotlinOptions.freeCompilerArgs += "-Xjvm-default=all-compatibility"
}

tasks.withType<GenerateMavenPom>().configureEach {
    dependsOn += tasks.test
}

publishing{
    publications {
        create<MavenPublication>("unbekannt"){
            pom {
                name.set(project.name)
                licenses{
                    license {
                        name.set("GNU Lesser General Public License")
                        url.set("https://www.gnu.org/licenses/lgpl-3.0.txt")
                    }
                }
                developers {
                    developer {
                        id.set("Unbekannt")
                        name.set("Unbekannt")
                    }
                }
                scm {
                    connection.set("scm:git:https://gitlab.com/Unbekannt/minecraft-electric-calculator.git")
                    url.set("https://gitlab.com/Unbekannt/minecraft-electric-calculator")
                }
            }
            from(components["java"])
            afterEvaluate {
                artifactId = tasks.jar.get().archiveBaseName.get()
            }
        }
    }
    repositories{
        if(project.hasProperty("my_maven_Dir")){
            maven {
                name = "Unbekannt-Maven"
                url = uri(project.property("my_maven_Dir")!!)
                credentials {
                    username = project.property("my_maven_username").toString()
                    password = project.property("my_maven_password").toString()
                }
            }
        }
        else{
            mavenLocal()
        }
    }
}