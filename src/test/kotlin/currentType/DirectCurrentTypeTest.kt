package currentType

import current.DirectCurrentType
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class DirectCurrentTypeTest {

    private val type = DirectCurrentType()

    @Test
    fun testPowerDissipation(){
        Assertions.assertEquals(3025.toDouble(), type.getPowerDissipation(50.0, 1.21))
    }

    @Test
    fun testCurrent(){
        Assertions.assertEquals(190.0826446280992, type.getCurrent(230.0, 1.21))
    }

    @Test
    fun testVoltage(){
        Assertions.assertEquals(60.5, type.getVoltage(50.0, 1.21))
    }
}