/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package temperature

/**
 * An Helper Object for Converting Minecraft Biomes Temperatures to Kelvin
 * All Values are in Kelvin
 */
object Temperature {

    /**
     * Kelvin Value for Freezing Water
     */
    const val freezingPoint = 273

    /**
     * Kelvin Value for 20°C or 67.73F
     */
    const val midValue = 293

    /**
     * Mc Biome Value for freezing Water
     */
    const val mcFreezing = 0.15

    /**
     * Mc Plains Biome Value
     */
    const val mcMidValue = 0.8

    /**
     * Get the Temperature in Kelvin for a given Biome Temperature
     *
     * @param worldTemp minecraft Biome Temperature value
     * @return Temperature in Kelvin
     */
    fun getTemp(worldTemp: Float): Double {
        return (1.0/13.0) * (3489+400*worldTemp)
    }

    fun getMcTemp(kelvinTemp: Double): Float {
        return ((13*(kelvinTemp-273) + 60)/400).toFloat()
    }
}