package temprature

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import temperature.Temperature

class TemperatureTest {

    @Test
    fun testTemperature(){
        Assertions.assertEquals(293.0, Temperature.getTemp(0.8F))
    }

    @Test
    fun testGetMcTemperature() {
        Assertions.assertEquals(0.8F, Temperature.getMcTemp(293.0))
    }
}