/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package maschine

import current.DirectCurrentType
import current.ICurrentType

interface IMaschine {

    val currentType: DirectCurrentType

    val resistance: Double

    /**
     * Get the Loss for the Machine
     * @see ICurrentType.getPowerDissipation
     */
    fun getLosses(current: Double): Double {
        return currentType.getPowerDissipation(current, resistance)
    }

    /**
     * Get the Current for the Machine
     * @see ICurrentType.getCurrent
     */
    fun getCurrent(voltage: Double) : Double{
        return currentType.getCurrent(voltage, resistance)
    }

    /**
     * get the Voltage for the Maschine
     * @see ICurrentType.getVoltage
     */
    fun getVoltage(current: Double) : Double {
        return currentType.getVoltage(current, resistance)
    }


}