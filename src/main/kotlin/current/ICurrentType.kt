/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package current

interface ICurrentType {

    /**
     * Get the Loss
     *
     * @param current current current in the Entity
     * @param resistance the Resistance for the Entity
     * @return the Power loss
     */
    fun getPowerDissipation(current : Double, resistance : Double) : Double

    /**
     * get the current
     *
     * @param voltage current Voltage in the Entity
     * @param resistance the Resistance for the Entity
     * @return the Current
     */
    fun getCurrent(voltage: Double, resistance : Double) : Double

    /**
     * get the Volltage
     *
     * @param current current Current in the Entity
     * @param resistance the Resistance for the Entity
     * @return the Voltage
     */
    fun getVoltage(current: Double, resistance: Double): Double
}