/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package ressources

import temperature.Temperature

interface IResources {


    /**
     * Temperature Coefficient of the Material at 293 Kelvin
     */
    val temperatureCoefficient: Double

    /**
     * specific Electrical Resistance (rho) at 293 Kelvin
     */
    val specificElectricalResistance: Double

    /**
     * Get the Resistance for the given Resource with the given cross section and Temperature
     *
     * @param worldTemp the Float Value from Minecraft used in the Biomes
     * @param crossSection the cross section of the Cable
     * @return the Resistance
     */
    fun getResistanceForTemperature(worldTemp: Float, crossSection: Double): Double {
        val temps = Temperature
        val temp = temps.getTemp(worldTemp)
        val diff = temp - temps.midValue
        val resistanceForDiameter = getResistanceForDefaultTemperature(crossSection)

        val diffOhm = temperatureCoefficient*diff*resistanceForDiameter
        return resistanceForDiameter + diffOhm
    }


    fun getResistance(rho : Double, crossSection: Double): Double {
        return rho*1/crossSection
    }

    fun getResistanceForDefaultTemperature(crossSection: Double): Double {
        return getResistance(specificElectricalResistance, crossSection)
    }
    //Intensive Systemgrößen (mengen unabhängig)


    /*
     * Leiter
     *
     * TemperaturLeitwert
     * ElektrischenLeitwert
     * WiderStand(Beleg Widerstand/Strecke(R/M))
     * Kapazitätsbelag
     */
    /*
    * Isolierung
    *
    * TemperaturLeitwert
    * ElektrischenLeitwert (Spannungsbelastbarkeit (V/cm)
     */
}