/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package ressources

import kotlin.math.pow

/**
 * This Enum has all Materials that are in Minecraft and some standard Values
 * form them that can be used
 */
enum class StandardResources : IResources {
    IRON{
        override val temperatureCoefficient: Double = 6.57
        override val specificElectricalResistance: Double = 1.0
    },
    LAPISLAZULI{
        override val temperatureCoefficient: Double
            get() = TODO("Not yet implemented")
        override val specificElectricalResistance: Double
            get() = TODO("Not yet implemented")
    },
    GOLD{
        override val temperatureCoefficient: Double = 3.7
        override val specificElectricalResistance: Double = 2.214
    },
    REDSTONE{
        override val temperatureCoefficient: Double
            get() = TODO("Not yet implemented")
        override val specificElectricalResistance: Double
            get() = TODO("Not yet implemented")
    },
    DIAMOND{
        override val temperatureCoefficient: Double = 0.0
        override val specificElectricalResistance: Double = 10 * (10.0.pow(8))
    },
    EMERALD {
        override val temperatureCoefficient: Double
            get() = TODO("Not yet implemented")
        override val specificElectricalResistance: Double
            get() = TODO("Not yet implemented")
    },
    NETHER_QUARZ {
        override val temperatureCoefficient: Double
            get() = TODO("Not yet implemented")
        override val specificElectricalResistance: Double
            get() = TODO("Not yet implemented")
    },
    NETHERITE {
        override val temperatureCoefficient: Double
            get() = TODO("Not yet implemented")
        override val specificElectricalResistance: Double
            get() = TODO("Not yet implemented")
    },
    COPPER {
        override val temperatureCoefficient: Double = 0.393
        override val specificElectricalResistance: Double = 1.8
    },
    GLAS{
        override val temperatureCoefficient: Double = 0.0
        override val specificElectricalResistance: Double = 1000000000000000000.0

    }
}