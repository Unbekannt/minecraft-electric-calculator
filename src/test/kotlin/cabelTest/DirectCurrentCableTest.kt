package cabelTest

import cabels.DirectCurrentCable
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import ressources.StandardResources

class DirectCurrentCableTest {

    private val cable = DirectCurrentCable(StandardResources.COPPER, null, 2, 1.5, 0.8F)

    @Test
    fun isResistanceRight(){
        Assertions.assertEquals(1.2, cable.resistance)
    }
}