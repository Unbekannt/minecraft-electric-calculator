/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cabels

import current.DirectCurrentType
import current.ICurrentType
import ressources.IResources

class DirectCurrentCable(
    override val conductor: IResources,
    override val insulation: IResources?,
    override val isolationThickness: Int,
    override val cableCrossSection: Double,
    worldTemp: Float
) : ICable {
    override var currentType: ICurrentType = DirectCurrentType()
    override val resistance: Double = conductor.getResistanceForTemperature(worldTemp, cableCrossSection)


}