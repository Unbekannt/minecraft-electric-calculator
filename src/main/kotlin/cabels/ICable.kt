/*
 *     This file is part of Minecraft-Electric-Calculator.
 *
 *     Minecraft-Electric-Calculator is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Lesser General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Minecraft-Electric-Calculator is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Lesser General Public License for more details.
 *
 *     You should have received a copy of the GNU Lesser General Public License
 *     along with Minecraft-Electric-Calculator.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package cabels

import current.ICurrentType
import ressources.IResources

interface ICable {

    /**
     * The Current Type DC/AC
     */
    var currentType : ICurrentType

    /**
     * Material used for the conductor
     */
    val conductor : IResources

    /**
     * Material used for the insulation
     */
    val insulation : IResources?

    /**
     * the thickness of the isolation
     */
    val isolationThickness : Int

    /**
     * cross Section of the Cable in square millimeter
     */
    val cableCrossSection : Double

    /**
     * the resistance of the Cable
     *
     * Must be set with the [IResources.getResistance] from the conductor
     */
    val resistance: Double

    /**
     * Get the Loss for the Cable
     * @see ICurrentType.getPowerDissipation
     */
    fun getLosses(current: Double): Double {
        return currentType.getPowerDissipation(current, resistance)
    }

    /**
     * Get the Current for the Cable
     * @see ICurrentType.getCurrent
     */
    fun getCurrent(voltage: Double) : Double{
        return currentType.getCurrent(voltage, resistance)
    }

    /**
     * get the Voltage
     * @see ICurrentType.getVoltage
     */
    fun getVoltage(current: Double) : Double {
        return currentType.getVoltage(current, resistance)
    }


    /*
     * Kabel
     * MaximalTemperatur umgebung
     * Verlustleistung
     *      ohne Verlust AC
     *      Dielektrische Verluste DC
     *      Blindleistungsverluste
     *
     *  PI Ersatzschaltbild (tabellen auslesbar) AC (erstmal nicht einbauen)
     */

    /*
     * Kabel Größen
     *
     * Isolirungsdicke -> MaxSpannung (mehr iso > mehr spannung)
     *                      MaxStrom (mehr iso < weniger strom)
     *
     * Leiterquerschnitt -> Max Strom mehr durchschnitt mehr strom
     *
     * Strombelastbarkeit -> Umgebung abhängig (Temp, Umgebung)
     *
     * Allgemein max umgebungstemperaturen (-5 - 90 °C)
     *
     */

}